-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
supports-screens
ADDED from AndroidManifest.xml:7:5
	android:resizeable
		ADDED from AndroidManifest.xml:11:9
	android:largeScreens
		ADDED from AndroidManifest.xml:9:9
	android:anyDensity
		ADDED from AndroidManifest.xml:8:9
	android:normalScreens
		ADDED from AndroidManifest.xml:10:9
	android:smallScreens
		ADDED from AndroidManifest.xml:12:9
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:17:5
	android:required
		ADDED from AndroidManifest.xml:19:9
	android:name
		ADDED from AndroidManifest.xml:18:9
uses-feature#android.hardware.camera.autofocus
ADDED from AndroidManifest.xml:20:5
	android:required
		ADDED from AndroidManifest.xml:22:9
	android:name
		ADDED from AndroidManifest.xml:21:9
uses-feature#android.hardware.camera.front
ADDED from AndroidManifest.xml:23:5
	android:required
		ADDED from AndroidManifest.xml:25:9
	android:name
		ADDED from AndroidManifest.xml:24:9
uses-feature#android.hardware.camera.front.autofocus
ADDED from AndroidManifest.xml:26:5
	android:required
		ADDED from AndroidManifest.xml:28:9
	android:name
		ADDED from AndroidManifest.xml:27:9
uses-feature#android.hardware.sensor.gyroscope
ADDED from AndroidManifest.xml:29:5
	android:required
		ADDED from AndroidManifest.xml:31:9
	android:name
		ADDED from AndroidManifest.xml:30:9
application
ADDED from AndroidManifest.xml:33:5
MERGED from com.android.support:appcompat-v7:21.0.2:16:5
MERGED from com.android.support:support-v4:21.0.2:16:5
	android:label
		ADDED from AndroidManifest.xml:35:9
	android:icon
		ADDED from AndroidManifest.xml:34:9
activity#org.opencv.samples.imagemanipulations.ImageManipulationsActivity
ADDED from AndroidManifest.xml:37:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:41:13
	android:label
		ADDED from AndroidManifest.xml:40:13
	android:configChanges
		ADDED from AndroidManifest.xml:39:13
	android:name
		ADDED from AndroidManifest.xml:38:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:42:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:43:17
	android:name
		ADDED from AndroidManifest.xml:43:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:44:27
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from image-manipulations:OpenCVLibrary249:unspecified:7:5
MERGED from com.android.support:appcompat-v7:21.0.2:15:5
MERGED from com.android.support:support-v4:21.0.2:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
