package org.opencv.samples.imagemanipulations;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.samples.imagemanipulations.custom.gui.VerticalSeekBar;
import org.opencv.samples.imagemanipulations.utils.Feedbacks;
import org.opencv.video.Video;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ImageManipulationsActivity extends Activity implements CvCameraViewListener2, SensorEventListener {

    private static final String TAG = "OCVSample::Activity";
    public Mode viewMode = Mode.VIEW;
    Scalar colorRed;
    Mat matThis;
    Mat matPrev;
    Mat submatThis;
    Mat submatPrev;
    MatOfFloat mMOFerr;
    MatOfByte mMOBStatus;
    MatOfPoint corners;
    MatOfPoint2f matOfFeatures2fPrev;
    MatOfPoint2f matOfFeatures2fThis;
    MatOfPoint2f matOfFeatures2fSafe;
    List<Point> cornersThis;
    List<Point> cornersPrev;
    List<Byte> byteStatus;
    Point pt1, pt2;
    int iLineThickness = 3;
    int maxCorners = 23; // era 40
    double roiHHalfPercentage = 0.4;
    double roiWHalfPercentage = 0.4;
    double planeDistance = 0.7;
    private SensorManager mSensorManager;
    private Sensor mAccelerationSensor;
    DisplayMetrics metrics;
    VerticalSeekBar sbRoiH;
    RadioButton rbClosest, rbAverage;
    SeekBar sbRoiW;
    EditText etDistance, etWidth;
    Button btnCalculate;
    RelativeLayout rlCalibration;
    double dpi;
    boolean averageAlert = false;

    // Movement
    private double mVPx = 0;
    private double mVPy = 0;
    private double mVPV0x = 0;
    private double mVPV0y = 0;
    private double mVPAccx = 0;
    private double mVPAccy = 0;
    private long mLastDiscontinuityTime = 0;
    private double maxAccx = 0;
    private double maxAccy = 0;
    private double mAverageDistance = -1;

    // Frames Movement
    private double mLastFrameX = 0;
    private double mLastFrameY = 0;
    private long mLastFrameTimestamp = 0;

    private MenuItem mItemPreviewRGBA;
    private MenuItem mItemPreviewFlow;
    private MenuItem mItemPreviewCalibration;
    private CameraBridgeViewBase mOpenCvCameraView;
    private Mat mIntermediateMat;
    private AlertLevel proximityAlert = AlertLevel.FAR;

    public enum Mode{
        VIEW,
        CALIBRATION,
        DETECTION
    }

    public enum AlertLevel{

        FAR(new Scalar(0, 255, 0)),
        APPROACHING(new Scalar(100, 255, 0), 1000, 200),
        NEAR(new Scalar(255, 255, 0), 500, 200),
        CLOSE(new Scalar(255, 100, 0), 500, 500),
        COLLISION(new Scalar(255, 0, 0), 100, 1000);

        long[] pattern;
        Scalar color;

        AlertLevel(Scalar color, long...pattern){
            this.pattern = pattern;
            this.color = color;
        }

        public static AlertLevel getEvaluateDistance(double distance){
            if(distance > 5) return FAR;
            if(distance > 2) return APPROACHING;
            if(distance > 1) return NEAR;
            if(distance > 0.5) return CLOSE;
            else return COLLISION;
        }

    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.e(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public ImageManipulationsActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.image_manipulations_surface_view);

        rlCalibration = (RelativeLayout) findViewById(R.id.rlCalibration);
        etDistance = (EditText) findViewById(R.id.etDistance);
        etWidth = (EditText) findViewById(R.id.etWidth);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double redWidth = dpiToMeters(100);
                if (!etWidth.getText().toString().isEmpty() &&
                        !etDistance.getText().toString().isEmpty()) {
                    planeDistance = (redWidth / Double.parseDouble(etWidth.getText().toString())) *
                            Double.parseDouble(etDistance.getText().toString());
                    Toast.makeText(ImageManipulationsActivity.this, "The calculated image plane distance is " + planeDistance, Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(ImageManipulationsActivity.this, "Please insert object width and distance", Toast.LENGTH_SHORT).show();
                }
            }
        });
        sbRoiH = (VerticalSeekBar) findViewById(R.id.sbRoi);
        sbRoiH.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                roiHHalfPercentage = ((double) progress / 100) + 0.1;
                matThis = new Mat();
                matPrev = new Mat();
                submatThis = new Mat();
                submatPrev = new Mat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbRoiW = (SeekBar) findViewById(R.id.sbRoiW);
        sbRoiW.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                roiWHalfPercentage = ((double)progress/100) + 0.1;
                matThis = new Mat();
                matPrev = new Mat();
                submatThis = new Mat();
                submatPrev = new Mat();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rbClosest = (RadioButton) findViewById(R.id.rbClose);
        rbAverage = (RadioButton) findViewById(R.id.rbAverage);

        rbClosest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                averageAlert = !isChecked;
            }
        });

        rbAverage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                averageAlert = isChecked;
            }
        });

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.image_manipulations_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mSensorManager.registerListener(this, mAccelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);

        metrics = getResources().getDisplayMetrics();
        dpi = (int)metrics.density;

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
        Feedbacks.stopVibrator();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.i(TAG, "called onCreateOptionsMenu");
        mItemPreviewRGBA = menu.add("Preview RGBA");
        mItemPreviewFlow = menu.add("OptFlow");
        mItemPreviewCalibration = menu.add("Calibration");
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);

        if (item == mItemPreviewRGBA) {
            toggleCalibrationInterface(false);
            viewMode = Mode.VIEW;
            Feedbacks.stopVibrator();
        }

        if (item == mItemPreviewFlow) {
            toggleCalibrationInterface(false);
            viewMode = Mode.DETECTION;
            Feedbacks.stopVibrator();
        }

        if (item == mItemPreviewCalibration) {
            toggleCalibrationInterface(true);
            viewMode = Mode.CALIBRATION;
            Feedbacks.stopVibrator();
        }

        return true;

    }

    public void onCameraViewStarted(int width, int height) {

        mIntermediateMat = new Mat();

        colorRed = new Scalar(255, 0, 0, 255);

        matThis = new Mat();
        matPrev = new Mat();
        submatThis = new Mat();
        submatPrev = new Mat();

        mMOFerr = new MatOfFloat();
        mMOBStatus = new MatOfByte();
        corners = new MatOfPoint();

        matOfFeatures2fPrev = new MatOfPoint2f();
        matOfFeatures2fThis = new MatOfPoint2f();
        matOfFeatures2fSafe = new MatOfPoint2f();

        cornersThis = new ArrayList<>();
        cornersPrev = new ArrayList<>();

        byteStatus = new ArrayList<>();

    }

    public void onCameraViewStopped() {
        // Explicitly deallocate Mats
        if (mIntermediateMat != null)
            mIntermediateMat.release();

        mIntermediateMat = null;
    }

    public Mat getSubMat(Size sizeRgba, Mat mat) {

        int y_start = (int) (sizeRgba.height / 2) - (int)Math.floor(sizeRgba.height * roiHHalfPercentage);
        int y_end = (int) (sizeRgba.height / 2) + (int) Math.floor(sizeRgba.height * roiHHalfPercentage);
        int x_start = (int) (sizeRgba.width / 2) - (int)Math.floor(sizeRgba.width * roiWHalfPercentage);
        int x_end = (int) (sizeRgba.width / 2) + (int)Math.floor(sizeRgba.width * roiWHalfPercentage);

        return mat.submat(y_start, y_end, x_start, x_end);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // when a new acceleration evaluation is available, we start a new uniformly accelerated motion
        long currentTime = new Date().getTime();
        // we evaluate the time delta from last acceleration reading
        double t = ((double)(currentTime - mLastDiscontinuityTime))/1000;
        mLastDiscontinuityTime = currentTime;
        // based on the time delta we evaluate current speeds and positions
        mVPx = mVPx + (mVPV0x * t) + 0.5 * mVPAccx * Math.pow(t, 2);
        mVPy = mVPy + (mVPV0y * t) + 0.5 * mVPAccy * Math.pow(t, 2);

        mVPV0x = mVPV0x + mVPAccx * t;
        mVPV0y = mVPV0y + mVPAccy * t;

        if(mVPAccx == maxAccx && event.values[0] < maxAccx){
            mVPV0x = 0;
            maxAccx = 0;
        }
        if(mVPAccy == maxAccy && event.values[0] < maxAccy){
            mVPV0y = 0;
            maxAccy = 0;
        }
        // we will use these positions and speeds as starting speeds and positions for the next UAM with
        // the new acceleration reading
        mVPAccx = event.values[0];
        if(Math.abs(mVPAccx) < 0.4) mVPAccx = 0;
        mVPAccy = event.values[1];
        if(Math.abs(mVPAccy) < 0.4) mVPAccy = 0;

        if(maxAccx < mVPAccx) maxAccx = mVPAccx;
        if(maxAccy < mVPAccy) maxAccy = mVPAccy;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        Log.i(TAG, "called onCameraFrame");

        Mat rgba = inputFrame.rgba();
        Size sizeRgba = rgba.size();

        double halfFrameWidth = sizeRgba.width/2;
        double halfFrameHeight = sizeRgba.height/2;
        int count = 0;

        MatOfPoint corners = new MatOfPoint();

        switch (viewMode) {

            case DETECTION:

                if(!matThis.empty() &&
                        !matOfFeatures2fThis.empty()){
                    // if current matix is not empty, then override prev matrix with it
                    matThis.copyTo(matPrev);
                    // same for the features matrix
                    matOfFeatures2fThis.copyTo(matOfFeatures2fPrev);
                }
                // convert rgb image to grayscale
                Imgproc.cvtColor(rgba, matThis, Imgproc.COLOR_RGBA2GRAY);
                // consider only the submat inside the current ROI
                submatThis = getSubMat(sizeRgba, matThis);
                // find good features in current submat
                Imgproc.goodFeaturesToTrack(submatThis, corners, maxCorners, 0.05, 20);
                // extract the array of feature points
                matOfFeatures2fThis.fromArray(corners.toArray());
                // in case this was the first iteration, then prev matrixes are empty thus no comparation can be performed
                // then skip the rest of the code for this iteration
                if(matPrev.empty() ||
                        matOfFeatures2fPrev.empty()) break;
                // if prev matrix is not empty then consider only the submatrix inside the current ROI
                submatPrev = getSubMat(sizeRgba, matPrev);
                // now compute the optical flows form previous features to new ones
                MatOfPoint2f matMovedFeatures2f = new MatOfPoint2f();
                Video.calcOpticalFlowPyrLK(submatPrev, submatThis, matOfFeatures2fPrev, matMovedFeatures2f, mMOBStatus, mMOFerr);
                // get a list of previous and moved features
                cornersPrev = matOfFeatures2fPrev.toList();
                cornersThis = matMovedFeatures2f.toList();
                // also the status which tells if a flow was found for the relative feature
                byteStatus = mMOBStatus.toList();

                // time since last discontinuity
                long currentTime = new Date().getTime();
                double t = ((double)(currentTime - mLastDiscontinuityTime))/1000;
                // evaluate current position of the device using uniformly accelerated motion equations
                double cVPx = mVPx + (mVPV0x*t) + (0.5 * mVPAccx*Math.pow(t,2));
                double cVPy = mVPy + (mVPV0y*t) + (0.5 * mVPAccy*Math.pow(t,2));
                // evaluate device movement by applying Pitagora to the current and previous positions
                double devMov = Math.sqrt(
                        Math.pow(cVPx - mLastFrameX, 2) +
                                Math.pow(cVPy - mLastFrameY, 2)
                );

                mLastFrameX = cVPx;
                mLastFrameY = cVPy;

                double sum = 0;

                HashMap<Double, Point> points = new HashMap<>();
                ArrayList<Double> distances = new ArrayList<>();

                // for each feature
                for (int i = 0; i < byteStatus.size(); i++) {
                    // only if a flow was found
                    if (byteStatus.get(i) == 1 &&
                            devMov != 0) {
                        // increase the found flows count
                        count++;
                        // evaluate current feature position
                        pt1 = new Point(
                                cornersThis.get(i).x + halfFrameWidth - (int)Math.floor(sizeRgba.width * roiWHalfPercentage),
                                cornersThis.get(i).y + halfFrameHeight - (int)Math.floor(sizeRgba.height * roiHHalfPercentage)
                        );
                        // evaluate previous feature position
                        pt2 = new Point(
                                cornersPrev.get(i).x + halfFrameWidth - (int)Math.floor(sizeRgba.width * roiWHalfPercentage),
                                cornersPrev.get(i).y + halfFrameHeight - (int)Math.floor(sizeRgba.height * roiHHalfPercentage)
                        );
                        // evaluate with Pitagora the feature movement
                        double featMov = Math.sqrt(
                                Math.pow(pt1.x - pt2.x, 2) +
                                        Math.pow(pt1.y - pt2.y, 2)
                        );
                        // convert pixel evaluation of feature movement to meters
                        double meterFeatureMovement = dpiToMeters(featMov);
                        // evaluate distance of the feature from the camera using the given proportion
                        double distance = (devMov / meterFeatureMovement) * planeDistance;
                        // accumulate the distance (to find the mean)
                        sum += distance;
                        // store the distance (to sort distances)
                        distances.add(distance);
                        // keep track of the relation distance - feature
                        points.put(distance, pt1);
                        // evaluate alert level based on distance of the feature
                        AlertLevel level = AlertLevel.getEvaluateDistance(distance);
                        // paint a line between the prev and current positions of the feature
                        Core.line(rgba, pt1, pt2, level.color, iLineThickness);

                    }

                }

                Collections.sort(distances);
                // for every distance in distance buffer
                for(Double distance : distances){
                    // if there is a feature associated to it
                    if(points.containsKey(distance)) {
                        // then evaluate its alert level
                        AlertLevel level = AlertLevel.getEvaluateDistance(distance);
                        // and paint a circle on the screen whose diameter is bigger the closest the feature is
                        // and whose color is given by the alert level it falls in
                        Core.circle(rgba, points.get(distance), (distances.size() - distances.indexOf(distance)), level.color, iLineThickness - 1);
                    }
                }
                // find the frame center
                Point frameCenter = new Point(halfFrameWidth, halfFrameHeight);
                // find average distance
                mAverageDistance = sum / count;
                // if the user chose to be alerted based on the feature distances average
                if(averageAlert){
                    // evaluate the distances average
                    double subSum = 0;
                    ArrayList<Double> subDists = distances;
                    if(distances.size() > 2) subDists = new ArrayList<>(subDists.subList(1, subDists.size() - 2));
                    for(double distance : subDists){
                        subSum += distance;
                    }
                    double subAVG = subSum / subDists.size();
                    if(distances.size() > 0) {
                        // evaluate alert based on the average
                        proximityAlert = AlertLevel.getEvaluateDistance(subAVG);
                        Core.circle(rgba, frameCenter, 10, proximityAlert.color, iLineThickness + 7);
                    }else proximityAlert = AlertLevel.FAR;
                }else {
                    // else use the first distance in the sorted distances buffer (the closest) to evaluate alert level
                    if (distances.size() > 0) {
                        proximityAlert = AlertLevel.getEvaluateDistance(distances.get(0));
                        Core.circle(rgba, frameCenter, 10, proximityAlert.color, iLineThickness + 7);
                    } else proximityAlert = AlertLevel.FAR;
                }

                break;

        }

        if(count == 0){
            // if there are no features, then paint a blue square around the ROI
            Core.rectangle(
                    rgba,
                    new Point(
                            halfFrameWidth - (int) Math.floor(sizeRgba.width * roiWHalfPercentage),
                            halfFrameHeight - (int) Math.floor(sizeRgba.height * roiHHalfPercentage)
                    ),
                    new Point(
                            halfFrameWidth + (int) Math.floor(sizeRgba.width * roiWHalfPercentage),
                            halfFrameHeight + (int) Math.floor(sizeRgba.height * roiHHalfPercentage)
                    ),
                    new Scalar(0, 0, 255),
                    2
            );
        }else {
            // otherwise paint a rectangle around the ROI colored based on the alert level
            Core.rectangle(
                    rgba,
                    new Point(
                            halfFrameWidth - (int) Math.floor(sizeRgba.width * roiWHalfPercentage),
                            halfFrameHeight - (int) Math.floor(sizeRgba.height * roiHHalfPercentage)
                    ),
                    new Point(
                            halfFrameWidth + (int) Math.floor(sizeRgba.width * roiWHalfPercentage),
                            halfFrameHeight + (int) Math.floor(sizeRgba.height * roiHHalfPercentage)
                    ),
                    proximityAlert.color,
                    2
            );
        }

        if(mVPV0y != 0 ||
                mVPV0x != 0 ||
                mVPAccx != 0 ||
                mVPAccy != 0){
            // if the device is movin send vibrationg feedback based on the alert level
            Feedbacks.vibrate(this, proximityAlert.pattern);
        }else{
            // otherwise stop the virbarions
            Feedbacks.stopVibrator();
        }
        // return the whole image
        return rgba;

    }

    private void toggleCalibrationInterface(boolean visible){
        if(visible){
            rlCalibration.setVisibility(View.VISIBLE);
        }else{
            rlCalibration.setVisibility(View.INVISIBLE);
        }
    }

    private double dpiToMeters(double dpiMeasure){
        return ((dpiMeasure * 0.5) / dpi) / 1000;
    }

}