package org.opencv.samples.imagemanipulations.utils;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

public class Feedbacks {

    private static Vibrator mVibrator;

    public static void vibrate(Context context, long[] pattern){
        if(mVibrator == null)
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if(pattern != null && pattern.length > 0) {
           mVibrator.vibrate(pattern, 0);
        }
    }

    public static void stopVibrator(){
        if(mVibrator != null) mVibrator.cancel();
    }

}
